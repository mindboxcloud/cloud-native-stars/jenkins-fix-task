
package builder

class JavaBuilder extends AbstractBuilder {
    
    @Override
    void build(){
        sh "go build main"
    }

    @Override
    void test(){
        sh "go test"
    }

    @Override
    void upload(){
    }
}
