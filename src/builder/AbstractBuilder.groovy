package builder

abstract class AbstractBuilder {
  static AbstractBuilder createBuilder(String type){
      switch (type){
          case "Go":
            return new GoBuilder()
            break
          case "Java":
            return new JavaBuilder()
            break
          case "NodeJS":
            return new NodeJSBuilder()
            break
      }
  }

  abstract void build()

  abstract void test()

  abstract void upload()
}
