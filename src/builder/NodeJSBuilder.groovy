package builder

class JavaBuilder extends AbstractBuilder {
    
    @Override
    void build(){
        sh "npm install && npm build"
    }

    @Override
    void test(){
        sh "npm install && npm test"
    }

    @Override
    void upload(){
        sh "npm build_image"
    }
}
