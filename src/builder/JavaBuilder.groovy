package builder

class JavaBuilder extends AbstractBuilder {
    
    @Override
    void build(){
        sh "./gradlew build"
    }

    @Override
    void test(){
        sh "./gradlew test"
    }

    @Override
    void upload(){
        sh "./gradlew buildImage"
    }
}
