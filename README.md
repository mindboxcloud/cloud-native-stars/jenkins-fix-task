# Task: code to fix part of Jenkins Shared Library

The task is to fix part of Jenkins Shared Library code that provide universal code builder depends on application type. 
Code isn't working cause of some mistakes made in how code is used. Also few things are lost like stages in pipeline script.
Possible also gradle configuration is failed as is copied from more functional library ;) 

Usage of this code should look like:
```groovy
@Library('universalCodeBuilder@master') _

codeBuilder {
    appType = "Go"
    // other params if needed
}

```

Task is to fix and improve this code to this form as is possible with simple configuration (can be extended and different based on app type) build this 3 projects:

- https://github.com/gothinkster/node-express-realworld-example-app
- https://github.com/gothinkster/golang-gin-realworld-example-app
- https://github.com/gothinkster/spring-boot-realworld-example-app

As output is required to present pipelines based on this code that build this projects.
How you will setup and use this code is your choice, eg. you can choose between implementing support for Gradle builder in container or you can use Jenkins plugin for it. Also what Jenkins you will use is your choice. 

Final efect should be 30 minute presentation. 
```

