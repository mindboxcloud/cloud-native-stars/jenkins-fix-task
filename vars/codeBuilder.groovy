import builder

def call(Closure body){
    def pipelineParams = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = pipelineParams
    body()

    env.APP_TYPE = pipelineParams.appType
    builder.AbstractBuilder builder = builder.AbstractBuilder.createBuilder(env.APP_TYPE)
    builder.build()
    builder.test()
    builder.upload()
    println "code builded!"
}
